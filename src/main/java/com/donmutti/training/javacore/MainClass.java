package com.donmutti.training.javacore;

import java.io.IOException;

/**
 * @author dmitry.petukhov
 */
public class MainClass {

    // will thrown ExceptionInInitializerError
    static {
        int a = 1 / 0;
    }

    public static void main(String[] args) throws IOException {
        System.out.println("Training / Java Core");
    }
}
