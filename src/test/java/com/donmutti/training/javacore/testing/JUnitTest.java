package com.donmutti.training.javacore.testing;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.internal.matchers.Contains;

public class JUnitTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testExpectedExceptionRule() {
        thrown.expect(ArithmeticException.class);
        thrown.expectMessage(new Contains("zero"));
        int a = 1 / 0;
    }
}
