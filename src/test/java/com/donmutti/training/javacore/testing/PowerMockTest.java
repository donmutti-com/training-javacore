package com.donmutti.training.javacore.testing;

import com.donmutti.training.javacore.pojo.AbstractClass;
import com.donmutti.training.javacore.pojo.FinalClass;
import com.donmutti.training.javacore.pojo.Interface;
import com.donmutti.training.javacore.pojo.OrdinaryClass;
import com.donmutti.training.javacore.pojo.Pen;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.powermock.api.mockito.PowerMockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Interface.class, FinalClass.class, OrdinaryClass.class})
public class PowerMockTest {

    @Test
    public void testMockNonFinalClass() {
        Pen mock = mock(Pen.class);
        assertNotNull(mock);
    }

    @Test
    public void testMockFinalClass() {
        FinalClass mock = mock(FinalClass.class);
        assertNotNull(mock);
    }

    @Test
    public void testMockAbstractClass() {
        AbstractClass mock = mock(AbstractClass.class);
        assertNotNull(mock);
    }

    @Test
    public void testMockClassStaticMethod() throws Exception {
        mockStatic(OrdinaryClass.class);
        when(OrdinaryClass.doStaticBoolean()).thenReturn(false);
        assertFalse(OrdinaryClass.doStaticBoolean());
    }

    @Test
    public void testMockClassAbstractMethod() throws Exception {
        AbstractClass mock = mock(AbstractClass.class);
        when(mock.doAbstractBoolean()).thenReturn(false);
        assertFalse(mock.doAbstractBoolean());
    }

    @Test
    public void testMockClassFinalMethod() throws Exception {
        OrdinaryClass mock = mock(OrdinaryClass.class);
        when(mock.doFinalBoolean()).thenReturn(false);
        assertFalse(mock.doFinalBoolean());
    }

    @Test
    public void testMockInterfaceMethod() throws Exception {
        Interface mock = mock(Interface.class);
        when(mock.doBoolean()).thenReturn(false);
        assertFalse(mock.doBoolean());
    }

    @Test
    public void testMockInterfaceStaticMethod() throws Exception {
        mockStatic(Interface.class);
        when(Interface.doStaticBoolean()).thenReturn(false);
        assertFalse(Interface.doStaticBoolean());
    }

    @Test
    public void testMockInterfaceDefaultMethod() throws Exception {
        Interface mock = mock(Interface.class);
        when(mock.doDefaultBoolean()).thenReturn(false);
        assertFalse(mock.doDefaultBoolean());
    }
}

