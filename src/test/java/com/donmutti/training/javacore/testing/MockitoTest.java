package com.donmutti.training.javacore.testing;

import com.donmutti.training.javacore.pojo.AbstractClass;
import com.donmutti.training.javacore.pojo.FinalClass;
import com.donmutti.training.javacore.pojo.Interface;
import com.donmutti.training.javacore.pojo.OrdinaryClass;
import com.donmutti.training.javacore.pojo.Pen;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.exceptions.base.MockitoException;
import org.mockito.runners.MockitoJUnitRunner;
import org.powermock.api.mockito.PowerMockito;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MockitoTest {

    @Test
    public void testMockNonFinalClass() {
        Pen mock = mock(Pen.class);
        assertNotNull(mock);
    }

    @Test(expected = MockitoException.class)
    public void testMockFinalClass() {
        mock(FinalClass.class);
    }

    @Test
    public void testMockAbstractClass() {
        AbstractClass mock = PowerMockito.mock(AbstractClass.class);
        assertNotNull(mock);
    }

    @Test(expected = MockitoException.class)
    public void testMockClassStaticMethod() throws Exception {
        when(OrdinaryClass.doStaticBoolean()).thenReturn(false);
    }

    @Test
    public void testMockClassAbstractMethod() throws Exception {
        AbstractClass mock = mock(AbstractClass.class);
        when(mock.doAbstractBoolean()).thenReturn(false);
        assertFalse(mock.doAbstractBoolean());
    }

    @Test(expected = AssertionError.class)
    public void testMockClassFinalMethod() throws Exception {
        OrdinaryClass mock = mock(OrdinaryClass.class);
        when(mock.doFinalBoolean()).thenReturn(false);
        assertFalse(mock.doFinalBoolean());  // assertion should fail, because original non-mocked method is executed
    }

    @Test
    public void testMockInterfaceMethod() throws Exception {
        Interface mock = mock(Interface.class);
        when(mock.doBoolean()).thenReturn(false);
        assertFalse(mock.doBoolean());
    }

    @Test(expected = MockitoException.class)
    public void testMockInterfaceStaticMethod() throws Exception {
        when(Interface.doStaticBoolean()).thenReturn(false);
    }

    @Test
    public void testMockInterfaceDefaultMethod() throws Exception {
        Interface mock = mock(Interface.class);
        when(mock.doDefaultBoolean()).thenReturn(false);
        assertFalse(mock.doDefaultBoolean());
    }


}

