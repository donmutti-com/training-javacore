package com.donmutti.training.javacore.javase.java8;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.junit.Test;

import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author dpetuhov
 */
public class DateTimeTest {

    @Test
    public void testMonthDay() throws InterruptedException {
        MonthDay monthDay = MonthDay.now();
        assertEquals(monthDay.getMonth(), Month.of(monthDay.getMonthValue()));
    }

    @Test
    public void testDayOfWeek() throws InterruptedException {
        DayOfWeek dayOfWeek = LocalDate.now().getDayOfWeek();
        assertEquals(dayOfWeek.minus(dayOfWeek.getValue() - 1), DayOfWeek.MONDAY);
    }

    @Test
    public void testLocalDateTime() throws InterruptedException {
        LocalDateTime dateTime1 = LocalDateTime.now();
        Thread.sleep(1);
        LocalDateTime dateTime2 = LocalDateTime.now();
        assertTrue(dateTime1.isBefore(dateTime2));
        assertTrue(dateTime2.isAfter(dateTime1));
    }

    @Test
    public void testZonedDateTime() {
        ZoneId londonZone = ZoneId.of("Europe/London");
        ZoneId nyZone = ZoneId.of("America/New_York");

        // Compare zoned date/time
        ZonedDateTime londonZoned = ZonedDateTime.now(londonZone);
        ZonedDateTime nyZoned = londonZoned.withZoneSameInstant(nyZone);
        Duration duration = Duration.between(londonZoned, nyZoned);
        assertTrue(duration.isZero());

        // Compare local date/time
        LocalDateTime londonLocal = londonZoned.toLocalDateTime();
        LocalDateTime nyLocal = nyZoned.toLocalDateTime();
        duration = Duration.between(londonLocal, nyLocal);
        assertTrue(duration.isNegative());
    }

    @Test
    public void testChronoUnits() {
        assertEquals(Duration.ofDays(1), Duration.of(1, ChronoUnit.DAYS));
        assertEquals(Duration.ofHours(1), Duration.of(1, ChronoUnit.HOURS));
        assertEquals(Duration.ofSeconds(1), Duration.of(1, ChronoUnit.SECONDS));
    }

    @Test
    public void testConvertFromDateToLocalDateTime() {
        Date date = new Date();
        LocalDateTime dateTime = fromDate(date);
        assertTrue(equal(dateTime, date));
    }

    @Test
    public void testConvertFromLocalDateTimeToDate() {
        LocalDateTime dateTime = LocalDateTime.now();
        Date date = toDate(dateTime);
        assertTrue(equal(dateTime, date));
    }

    private Date toDate(LocalDateTime dateTime) {
        return Date.from(dateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    private LocalDateTime fromDate(Date date) {
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(date.getTime()), ZoneId.systemDefault());
    }

    private boolean equal(LocalDateTime dateTime, Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return new EqualsBuilder()
            .append(dateTime.getYear(), calendar.get(Calendar.YEAR))
            .append(dateTime.getMonthValue() - 1, calendar.get(Calendar.MONTH))
            .append(dateTime.getDayOfMonth(), calendar.get(Calendar.DAY_OF_MONTH))
            .append(dateTime.getHour(), calendar.get(Calendar.HOUR_OF_DAY))
            .append(dateTime.getMinute(), calendar.get(Calendar.MINUTE))
            .append(dateTime.getSecond(), calendar.get(Calendar.SECOND))
            .append(dateTime.getNano() / 1_000_000, calendar.get(Calendar.MILLISECOND))
            .build();
    }

}
