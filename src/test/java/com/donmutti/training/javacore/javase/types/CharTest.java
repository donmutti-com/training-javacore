package com.donmutti.training.javacore.javase.types;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertEquals;

/**
 * @author dpetuhov
 */
@RunWith(JUnit4.class)
public class CharTest {

    private static final Logger LOG = LogManager.getLogger();

    @Test
    public void testChar_IntString() {
        assertEquals('A' + "12", "A12");
    }

    @Test
    public void testString_Int() {
        assertEquals("A" + 12, "A12");
    }

    @Test
    public void testChar_IntChar_IntString() {
        assertEquals('A' + '1' + "2", "1142");
    }

    @Test
    public void testString_Char_Char() {
        assertEquals("A" + '\t' + '\u0003', "A\t\u0003");
    }

    @Test
    public void testString__Char_Char() {
        assertEquals("A" + ('\t' + '\u0003'), "A12");
    }

}