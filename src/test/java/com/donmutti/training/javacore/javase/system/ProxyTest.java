package com.donmutti.training.javacore.javase.system;

import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @author dmitry.petukhov
 */
interface MyInterface {
    String getName();

    void setName(String newName);
}

class MyClass implements MyInterface {

    private String name = "";

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }
}

class MyInvocationHandler implements InvocationHandler {

    private MyInterface proxied;

    MyInvocationHandler(MyInterface proxied) {
        this.proxied = proxied;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        return method.invoke(proxied, args);
    }
}

public class ProxyTest {

    @Test
    public void testProxy() {

        MyInterface myObject = new MyClass();
        MyInterface proxyObject = (MyInterface) Proxy.newProxyInstance(
                MyClass.class.getClassLoader(),
                new Class[]{MyInterface.class},
                new MyInvocationHandler(myObject)
        );

        proxyObject.setName("ProxyName");

        Assert.assertEquals(myObject.getName(), proxyObject.getName());
    }
}