package com.donmutti.training.javacore.javase.system;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author dpetuhov
 */
public class SystemPropertiesTest {

    @Test
    public void testMainSystemProperties() {
        String[] mainProps = {
            "file.separator",
            "java.class.path",
            "java.home",
            "java.vendor",
            "java.vendor.url",
            "java.version",
            "line.separator",
            "os.arch",
            "os.name",
            "os.version",
            "path.separator",
            "user.dir",
            "user.home",
            "user.name",
        };
        for (String prop : mainProps) {
            assertNotNull(String.format("Property \"%s\" is missing.", prop), System.getProperty(prop));
        }
    }

    @Test
    public void testChangeSystemProperty() {
        String name = "user.dir";
        String expected = "c:/SomeDir";
        System.setProperty(name, expected);
        String actual = System.getProperty(name);
        assertEquals(expected, actual);
    }
}
