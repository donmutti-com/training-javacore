package com.donmutti.training.javacore.javase.interfaces;

interface MyInterface2 {

    static int doStatic() {
        return 20;
    }

    default int doDefault() {
        return 21;
    }

    int doAbstract();
}
