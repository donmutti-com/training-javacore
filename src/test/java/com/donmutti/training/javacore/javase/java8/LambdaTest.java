package com.donmutti.training.javacore.javase.java8;

import com.donmutti.training.javacore.pojo.Person;
import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertTrue;

/**
 * Java 8 Lambdas test
 *
 * @author dpetuhov
 */
public class LambdaTest {

    private static final Logger LOG = LogManager.getLogger();

    private static final long ITEM_COUNT = 100;

    private List<Person> baseList = new ArrayList<>();

    @Before
    public void setUp() {
        populateList(baseList);
    }

    private void populateList(@NotNull List<Person> list) {
        Random rand = new Random(System.currentTimeMillis());
        list.clear();
        for (long i = 0; i < ITEM_COUNT; i++) {
            list.add(new Person((long) rand.nextInt(100), "item-" + i));
        }
    }

    @Test
    public void testSort() {
        // Sort list #1 (Comparator built as classical anonymous class )
        List<Person> list1 = new ArrayList<>(baseList);
        Collections.sort(list1, new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                return new CompareToBuilder()
                    .append(o1.getName(), o2.getName())
                    .append(o1.getId(), o2.getId())
                    .build();
            }
        });

        // Sort list #2 (Comparator built with lambda)
        List<Person> list2 = new ArrayList<>(baseList);
        Collections.sort(list2, (o1, o2) -> new CompareToBuilder()
            .append(o1.getName(), o2.getName())
            .append(o1.getId(), o2.getId())
            .build());

        // Sort list #3 (Comparator built with chain of static methods)
        List<Person> list3 = new ArrayList<>(baseList);
        list3.sort(Comparator.comparing(Person::getName, String::compareTo).thenComparingLong(Person::getId));

        // Assert that all three sorting results are equal
        boolean equal = list1.equals(list2) && list2.equals(list3);
        assertTrue("Sorting results differ!", equal);
    }

}
