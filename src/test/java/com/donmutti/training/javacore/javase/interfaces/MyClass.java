package com.donmutti.training.javacore.javase.interfaces;

class MyClass implements MyInterface, MyInterface2 {

    static int doStatic() {
        return 30;
    }

    @Override
    public int doDefault() {
        return MyInterface2.super.doDefault();
    }

    @Override
    public int doAbstract() {
        return 32;
    }
}
