package com.donmutti.training.javacore.javase.java8;

import com.donmutti.training.javacore.pojo.Person;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static org.junit.Assert.assertTrue;

/**
 * @author dpetuhov
 */
public class SupplierTest {

    @Test
    public void testSupplierAsConstructorReference() {
        Supplier<Person> supplier = Person::new;
        List<Person> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            list.add(supplier.get());
        }
        Long distinctCount = list.stream()
            .distinct()
            .collect(Collectors.counting());

        assertTrue(distinctCount == 1);
    }

    @Test
    public void testSupplierAsLambda() {
        List<Person> list = new ArrayList<>();
        Supplier<Person> supplier = () -> new Person((long) list.size(), "Person-" + list.size());
        for (int i = 0; i < 10; i++) {
            list.add(supplier.get());
        }
        Long distinctCount = list.stream()
            .distinct()
            .collect(Collectors.counting());

        assertTrue(distinctCount == list.size());
    }
}
