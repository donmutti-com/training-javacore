package com.donmutti.training.javacore.javase.java8;

import com.donmutti.training.javacore.pojo.Pen;
import org.junit.Test;

import java.awt.*;
import java.util.function.Function;

import static org.junit.Assert.assertEquals;

/**
 * @author dpetuhov
 */
public class FunctionTest {

    @Test
    public void testFunctionAsLambda() {
        Function<Pen, Color> function = (p) -> p.getColor();
        Color expected = Color.orange;
        Color actual = function.apply(new Pen(Color.orange, 100.0f));

        assertEquals(expected, actual);
    }

    @Test
    public void testFunctionAsMethodReference() {
        Function<Pen, Color> function = Pen::getColor;
        Color expected = Color.orange;
        Color actual = function.apply(new Pen(Color.orange, 100.0f));

        assertEquals(expected, actual);
    }
}
