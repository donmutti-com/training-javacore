package com.donmutti.training.javacore.javase.generics;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.lang.reflect.TypeVariable;

/**
 * @author dmitry.petukhov
 */
@RunWith(JUnit4.class)
public class GenericsTest {

    @Test
    public void testWildcardTypes() throws Exception {
        class Clazz<T> {}
        Clazz<?> ref1 = new Clazz<Integer>();
        Clazz<?> ref2 = new Clazz<Boolean>();

        Assert.assertEquals(ref1.getClass(), ref2.getClass());

        TypeVariable<? extends Class<? extends Clazz>>[] typeVar1 = ref1.getClass().getTypeParameters();
        TypeVariable<? extends Class<? extends Clazz>>[] typeVar2 = ref2.getClass().getTypeParameters();

        Assert.assertEquals(typeVar1, typeVar2);


    }
}

