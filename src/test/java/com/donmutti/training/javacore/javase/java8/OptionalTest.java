package com.donmutti.training.javacore.javase.java8;

import com.donmutti.training.javacore.pojo.Pen;
import org.junit.Test;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.junit.Assert.assertEquals;

/**
 * @author dpetuhov
 */
public class OptionalTest {

    private final Map<Color, Pen> pens = new HashMap<Color, Pen>() {{
        put(Color.red, new Pen(Color.red, 50.0f));
        put(Color.green, new Pen(Color.green, 33.3f));
        put(Color.blue, null);
    }};

    @Test
    public void testNullFilter() {
        final Color testColor = Color.blue;
        final Color defaultColor = null;
        Color penColor = Optional.ofNullable(pens.get(testColor))
            .filter(p -> p.getVolume() >= 50.0f)
            .map(Pen::getColor)
            .orElse(defaultColor);

        assertEquals(defaultColor, penColor);
    }

    @Test
    public void testNotNullFilterExisting() {
        final Color testColor = Color.red;
        final Color defaultColor = null;
        Color penColor = Optional.ofNullable(pens.get(testColor))
            .filter(p -> p.getVolume() >= 50.0f)
            .map(Pen::getColor)
            .orElse(defaultColor);

        assertEquals(testColor, penColor);
    }

    @Test
    public void testNotNullFilterNotExisting() {
        final Color testColor = Color.green;
        final Color defaultColor = null;
        Color penColor = Optional.ofNullable(pens.get(testColor))
            .filter(p -> p.getVolume() >= 50.0f)
            .map(Pen::getColor)
            .orElse(defaultColor);

        assertEquals(defaultColor, penColor);
    }

    @Test
    public void testFlatMapExisting() {
        final Color testColor = Color.green;
        final Color defaultColor = null;
        Color penColor = Optional.ofNullable(pens.get(testColor))
            .flatMap(pen -> Optional.ofNullable(pen.getColor()))
            .orElse(defaultColor);

        assertEquals(testColor, penColor);
    }

    @Test
    public void testFlatMapNotExisting() {
        final Color testColor = Color.blue;
        final Color defaultColor = null;
        Color penColor = Optional.ofNullable(pens.get(testColor))
            .flatMap(pen -> Optional.ofNullable(pen.getColor()))
            .orElse(defaultColor);

        assertEquals(defaultColor, penColor);
    }

    @Test
    public void testEmpty() throws Exception {
        final Color testColor = null;
        final Color defaultColor = Color.orange;
        Color penColor =
            pens.isEmpty() ?
                Optional.<Color>empty().get() :
                Optional.ofNullable(pens.get(testColor))
                    .map(Pen::getColor)
                    .orElse(defaultColor);

        assertEquals(defaultColor, penColor);
    }
}
