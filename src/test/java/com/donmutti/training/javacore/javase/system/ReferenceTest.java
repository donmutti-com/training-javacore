package com.donmutti.training.javacore.javase.system;

import org.junit.Test;

import java.awt.*;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @see <a href="http://javarevisited.blogspot.ru/2014/03/difference-between-weakreference-vs-softreference-phantom-strong-reference-java.html">Difference between WeakReference vs SoftReference vs PhantomReference vs Strong reference in Java</a>
 * @author dpetuhov
 */
public class ReferenceTest {

    /**
     * TODO: 15.03.2016 nedd to play with ReferenceQueue:
     * - https://habrahabr.ru/post/169883/
     * - http://stackoverflow.com/questions/10878012/using-referencequeue-and-weakreference
     */
    @Test
    public void testWeakReference() {
        final int MAX_GC_ITERATIONS = 1;
        final WeakReference<Object> weakRef = new WeakReference<>(new Object());

        // WeakReference loses its value after the very first GC
        int i = 0;
        while (weakRef.get() != null && i < MAX_GC_ITERATIONS) {
            i++;
            gcAndWaitABit();
        }
        assertNull(weakRef.get());
    }

    /**
     * @see <a href="https://docs.oracle.com/javase/7/docs/api/java/lang/ref/SoftReference.html">https://docs.oracle.com/javase/7/docs/api/java/lang/ref/SoftReference.html</a>
     */
    @Test
    public void testSoftReference() {
        final int MAX_GC_ITERATIONS = 10;
        final SoftReference<Object> softRef = new SoftReference<>(new Object());

        // SoftReference won't loose its value even after several GCs
        int i = 0;
        while (softRef.get() != null && i < MAX_GC_ITERATIONS) {
            i++;
            gcAndWaitABit();
        }
        assertNotNull(softRef.get());

        // But SoftReference will loose its value if JVM absolutely needs memory.
        // All soft references to softly-reachable objects are guaranteed to have been cleared before the virtual machine throws an OutOfMemoryError
        try {
            final List<Object[]> eatLotOfMemory = new ArrayList<>();
            int size;
            while (( size = Math.min((int)Runtime.getRuntime().freeMemory(), Integer.MAX_VALUE)) > 0) {
                eatLotOfMemory.add(new Object[size]);
            }
        } catch (OutOfMemoryError e) {
            assertNull(softRef.get());
        }
    }

    @Test
    public void testArrayCopy() {
        String srcString = "Rise and shine, Mr. Freeman!";
        byte[] srcBytes = srcString.getBytes();
        byte[] destBytes = new byte[srcBytes.length];
        System.arraycopy(srcBytes, 0, destBytes, 0, srcBytes.length);
        destBytes[18] = 's';
        String destString = new String(destBytes);
        assertEquals(srcString.replace('.', 's'), destString);
    }

    private void gcAndWaitABit() {
        final Runtime runtime = Runtime.getRuntime();
        runtime.runFinalization();
        runtime.gc();
        try {
            EventQueue.invokeAndWait(() -> {});
            Thread.sleep(1);
        } catch (InvocationTargetException | InterruptedException e) {
            e.printStackTrace();
        }
    }

}