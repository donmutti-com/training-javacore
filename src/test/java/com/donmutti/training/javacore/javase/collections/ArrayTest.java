package com.donmutti.training.javacore.javase.collections;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

/**
 * @author dpetuhov
 */
public class ArrayTest {

    private static final Logger LOG = LogManager.getLogger();

    private int[][] m = {{1, 2, 3}, {1, 2,}, {1},};

    @Test
    public void testArrayToString2() {
        m[0][0] += 2;
    }

    @Test
    public void testArrayToString() {
        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[i].length; j++) {
                m[i][j]++;
            }
        }
        String actual = Arrays.deepToString(m);
        String expected = "[[2, 3, 4], [2, 3], [2]]";
        assertEquals(expected, actual);
    }
}
