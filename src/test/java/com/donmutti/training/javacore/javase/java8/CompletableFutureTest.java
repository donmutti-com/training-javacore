package com.donmutti.training.javacore.javase.java8;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Function;

import static org.junit.Assert.assertTrue;

/**
 * @author dpetuhov
 */
public class CompletableFutureTest {

    /**
     * Downloads a content from given URL with 5-sec timeout
     */
    @Test
    public void testDownloadFromURL() {
        // Define a Function which internally uses CompletableFuture
        Function<URL, Object> contentProvider = url -> {
            // CompletableFuture
            CompletableFuture<Object> loader = CompletableFuture.supplyAsync(() -> {
                try {
                    return IOUtils.toString(url, Charset.forName("UTF-8"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            });

            // Function body
            try {
                return loader.get(5000, TimeUnit.MILLISECONDS);
            } catch (InterruptedException | ExecutionException | TimeoutException e) {
                e.printStackTrace();
            }
            return null;
        };

        // Function call
        Object content = null;
        try {
            String RESOURCE_URL = "http://www.google.com";
            content = contentProvider.apply(new URL(RESOURCE_URL));
            assertTrue(content != null);
            System.out.print(content);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

}
