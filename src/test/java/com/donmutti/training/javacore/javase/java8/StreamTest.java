package com.donmutti.training.javacore.javase.java8;

import com.donmutti.training.javacore.pojo.Person;
import com.donmutti.training.javacore.pojo.Pet;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

/**
 * @author dpetuhov
 */
public class StreamTest {

    private static final Logger LOG = LogManager.getLogger();

    private static final long ITEM_COUNT = 100;

    private List<Person> baseList = new ArrayList<>();

    @Before
    public void setUp() {
        givenBaseList(baseList);
    }

    private static void givenBaseList(List<Person> list) {
        Random random = new Random();
        for (long i = 0; i < ITEM_COUNT; i++) {
            Person person = new Person(i, "item-" + i);
            int petCount = random.nextInt(5);
            for (long j = 0; j < petCount; j++) {
                person.getPets().add(new Pet("Pet-" + j));
            }
            list.add(person);
        }
    }

    @Test
    public void testFilterCollect() {
        List<Person> list = baseList.stream()
            .filter(item -> item.getName().contains("1"))
            .filter(item -> item.getName().length() <= 7)
            .collect(Collectors.toList());

        assertEquals(19, list.size());
    }

    @Test
    public void testFilterFindFirst() {
        String actual = baseList.stream()
            .filter(item -> item.getName().contains("-9"))
            .map(Person::getName)
            .findFirst()
            .orElse(null);

        assertEquals("item-9", actual);
    }

    @Test
    public void testForEach() {
        baseList.stream().forEach(person -> { person.setName(StringUtils.reverse(person.getName())); });

        assertEquals("91-meti", baseList.get(19).getName());

    }

    @Test
    public void testMap() throws Exception {
        long totalLength = baseList.stream().map(person -> person.getClass().getName()).collect(Collectors.summingLong(String::length));

        assertEquals(4200, totalLength);
    }

    @Test
    public void testFlatMap() throws Exception {

        // Oldschool
        List<String> allPetNamesOldschool = new ArrayList<>();
        for (Person person: baseList) {
            for (Pet pet: person.getPets()) {
                allPetNamesOldschool.add(pet.getName());
            }
        }

        // Stream API
        List<String> allPetNames = baseList.stream()
            .flatMap(person -> person.getPets().stream())
            .map(Pet::getName)
            .collect(Collectors.toList());

        // Compare
        assertEquals(allPetNamesOldschool, allPetNames);
    }
}
