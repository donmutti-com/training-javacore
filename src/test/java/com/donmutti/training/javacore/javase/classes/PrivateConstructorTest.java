package com.donmutti.training.javacore.javase.classes;

import org.junit.Test;

public class PrivateConstructorTest {

    @Test
    public void testPrivateConstructorInheritance() {
        //Container.A a = new Container.A();
        Container.B b = new Container().new B();
    }

}


class Container {

    class A {
        private A() {
        }
    }

    class B extends A {
    }

}
