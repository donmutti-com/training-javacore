package com.donmutti.training.javacore.javase.concurrency;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author dpetuhov
 */
public class ThreadTest {

    private static final Logger LOG = LogManager.getLogger();

    // Constants
    private static final long COUNT = 10;      // Test number
    private static final long DELAY = 100;     // Production delay, msec

    @Test
    public void runVolatileCounterTest() {
        // Create a runnable
        Runnable r = new Runnable() {
            volatile int i = 0;

            @Override
            public void run() {
                while (i < COUNT) {
                    i++;
                    LOG.info("i: " + i);
                }
            }
        };

        // Run two threads
        // Run two threads
        Thread t1 = new Thread(r, "main");
        Thread t2 = new Thread(r, "Thread-2");
        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            LOG.error(e);
        }
    }

    @Test
    public void runCounterTest() {
        // Create a runnable
        Runnable r = new Runnable() {
            int i = 0;

            @Override
            public void run() {
                while (i < COUNT) {
                    i++;
                    LOG.info(Thread.currentThread().getName() + ": " + i);
                }
            }
        };

        // Run two threads
        Thread t1 = new Thread(r, "Thread-1");
        Thread t2 = new Thread(r, "Thread-2");
        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            LOG.error(e);
        }
    }

    @Test
    public void runThreadLocalCounterTest() {
        // Create a runnable
        Runnable r = new Runnable() {
            ThreadLocal<Integer> i = ThreadLocal.withInitial(() -> 0);

            @Override
            public void run() {
                while (i.get() < COUNT) {
                    i.set(i.get() + 1);
                    LOG.info(Thread.currentThread().getName() + ": " + i.get());
                }
            }
        };

        // Run two threads
        // Run two threads
        Thread t1 = new Thread(r, "Thread-1");
        Thread t2 = new Thread(r, "Thread-2");
        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            LOG.error(e);
        }
    }

    @Test
    public void runAtomicCounterTest() {
        // Create a runnable
        Runnable r = new Runnable() {
            AtomicInteger i = new AtomicInteger(0);

            @Override
            public void run() {
                while (i.get() < COUNT) {
                    int value = i.incrementAndGet();
                    LOG.info(Thread.currentThread().getName() + ": " + value);
                }
            }
        };

        // Run two threads
        Thread t1 = new Thread(r, "Thread-1");
        Thread t2 = new Thread(r, "Thread-2");
        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            LOG.error(e);
        }
    }

    @Test
    public void runConsumerProducerTest() {
        Queue<Message> queue = new LinkedList<>();
        Consumer consumer = new Consumer(queue);
        Producer producer = new Producer(queue);

        consumer.start();

        try {
            Thread.sleep(DELAY);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        producer.start();

        try {
            consumer.join();
            producer.join();
        } catch (InterruptedException e) {
            LOG.error(e);
        }
    }

    private static class Consumer extends Thread {

        private final Queue<Message> queue;

        public Consumer(Queue<Message> queue) {
            this.setName("Consumer");
            this.queue = queue;
        }

        @Override
        public void run() {
            LOG.info(getName() + " started.");

            // Consuming lifecycle
            while (!isInterrupted()) {
                synchronized (queue) {
                    try {
                        if (queue.isEmpty()) {
                            queue.wait();
                        }
                    } catch (InterruptedException e) {
                        LOG.error(e);
                    }

                    Message msg = queue.poll();
                    if (msg == null) {
                        LOG.info("Consumed: null, quitting consumer...");
                        interrupt();
                    } else {
                        LOG.info("Consumed:               " + msg);
                    }
                }
            }

            LOG.info("Consumer finished.");
        }
    }

    private static class Producer extends Thread {
        private final Queue<Message> queue;

        public Producer(Queue<Message> queue) {
            this.setName("Producer");
            this.queue = queue;
        }

        @Override
        public void run() {
            LOG.info(getName() + " started.");

            for (int i = 0, I_MAX = 10; i <= I_MAX; i++) {
                synchronized (queue) {
                    try {
                        sleep(DELAY);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    Message msg = null;
                    if (i != I_MAX) {
                        msg = new Message("Stuff #" + i);
                    }
                    queue.add(msg);
                    LOG.info("Produced: " + msg);
                    queue.notifyAll();
                }
            }

            LOG.info("Producer finished.");
        }
    }

    /**
     * Message class
     */
    private static class Message {
        private String text;

        public Message(String text) {
            this.text = text;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }
    }
}
