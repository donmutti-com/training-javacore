package com.donmutti.training.javacore.javase.collections;

import com.donmutti.training.javacore.pojo.Direction;
import com.donmutti.training.javacore.pojo.Pen;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import java.awt.*;
import java.util.*;
import java.util.concurrent.*;

/**
 * @author dpetuhov
 */
public class SetMapTest {

    private static final Logger LOG = LogManager.getLogger();

    @Test(expected = UnsupportedOperationException.class)
    public void testCopyOnWriteArrayList() {
        CopyOnWriteArrayList<Integer> list = new CopyOnWriteArrayList<>();
        list.addAll(Arrays.asList(1, 2, 3));

        Iterator<Integer> it = list.iterator();
        while (it.hasNext()) {
            it.next();
            it.remove(); // iterator doesn't support remove operation
        }
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testCopyOnWriteArraySet() {
        CopyOnWriteArraySet<Integer> set = new CopyOnWriteArraySet<>();
        set.addAll(Arrays.asList(1, 2, 3));

        Iterator<Integer> it = set.iterator();
        while (it.hasNext()) {
            it.next();
            it.remove(); // iterator doesn't support remove operation
        }
    }

    @Test
    public void testHashSet() {
        Set<Pen> set = new HashSet<>();
        set.addAll(Arrays.asList(null, new Pen(Color.red, 50.0f), new Pen(Color.red, 33.3f)));

        Iterator it = set.iterator();
        while (it.hasNext()) {
            it.next();
            it.remove();
        }
    }

    @Test(expected = NullPointerException.class)
    public void testTreeSet_Null() {
        Set<Pen> set = new TreeSet<>();
        set.add(null);  // null items are not allowed
    }

    @Test(expected = NullPointerException.class)
    public void testConcurrentSkipListSet_Null() {
        Set<Pen> set = new ConcurrentSkipListSet<>();
        set.add(null);  // null items are not allowed
    }

    @Test(expected = NullPointerException.class)
    public void testHashTable_NullKey() {
        Map<Integer, String> map = new Hashtable<>();
        map.put(null, "Null Key");  // null keys are not allowed
    }

    @Test(expected = NullPointerException.class)
    public void testHashTable_NullValue() {
        Map<Integer, String> map = new Hashtable<>();
        map.put(0, null); // null values are not allowed
    }

    @Test
    public void testHashMap() {
        Map<Integer, String> map = new HashMap<>();
        map.put(null, null);
        map.put(null, "Null Key");
        map.put(0, null);
        map.put(1, "First");
        map.put(2, "Second");
        map.put(3, "Third");
    }

    @Test
    public void testLinkedHashMap() {
        Map<Integer, String> map = new LinkedHashMap<>();
        map.put(null, null);
        map.put(null, "Null Key");
        map.put(0, null);
        map.put(1, "First");
        map.put(2, "Second");
        map.put(3, "Third");
    }

    @Test
    public void testIdentityHashMap() {
        Map<Integer, String> map = new IdentityHashMap<>();
        map.put(null, null);
        map.put(null, "Null Key");
        map.put(0, null);
        map.put(1, "First");
        map.put(2, "Second");
        map.put(3, "Third");
    }

    @Test
    public void testWeakHashMap() {
        Map<Integer, String> map = new WeakHashMap<>();
        map.put(null, null);
        map.put(null, "Null Key");
        map.put(0, null);
        map.put(1, "First");
        map.put(2, "Second");
        map.put(3, "Third");
    }

    @Test(expected = NullPointerException.class)
    public void testEnumMap_NullKey() {
        EnumMap<Direction, String> map = new EnumMap<>(Direction.class);
        map.put(null, "Null Key");  // null keys are not allowed
    }

    @Test
    public void testEnumMap() {
        EnumMap<Direction, String> map = new EnumMap<>(Direction.class);
        map.put(Direction.EAST, null);
        map.put(Direction.NORTH, "First");
        map.put(Direction.WEST, "Second");
        map.put(Direction.SOUTH, "Third");
    }

    @Test(expected = NullPointerException.class)
    public void testTreeMapNullKey() {
        Map<Integer, String> map = new TreeMap<>();
        map.put(null, "Null Key");  // null keys are not allowed
    }

    @Test
    public void testTreeMap() {
        Map<Integer, String> map = new TreeMap<>();
        map.put(0, null);
        map.put(1, "First");
        map.put(2, "Second");
        map.put(3, "Third");
    }

    @Test(expected = NullPointerException.class)
    public void testConcurrentHashMap_NullKey() {
        Map<Integer, String> map = new ConcurrentHashMap<>();
        map.put(null, "Null Key");  // null keys are not allowed
    }

    @Test(expected = NullPointerException.class)
    public void testConcurrentHashMap_NullValue() {
        Map<Integer, String> map = new ConcurrentHashMap<>();
        map.put(0, null); // null values are not allowed
    }

    @Test
    public void testConcurrentHashMap() {
        Map<Integer, String> map = new ConcurrentHashMap<>();
        map.put(1, "First");
        map.put(2, "Second");
        map.put(3, "Third");
    }

    @Test(expected = NullPointerException.class)
    public void testConcurrentSkipListMap_NullKey() {
        Map<Integer, String> map = new ConcurrentSkipListMap<>();
        map.put(null, "Null Key"); // null keys are not allowed
    }

    @Test(expected = NullPointerException.class)
    public void testConcurrentSkipListMap_NullValue() {
        Map<Integer, String> map = new ConcurrentSkipListMap<>();
        map.put(0, null); // null values are not allowed
    }

    @Test
    public void testConcurrentSkipListMap() {
        Map<Integer, String> map = new ConcurrentSkipListMap<>();
        map.put(1, "First");
        map.put(2, "Second");
        map.put(3, "Third");
    }

}
