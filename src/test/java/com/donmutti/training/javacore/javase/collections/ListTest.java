package com.donmutti.training.javacore.javase.collections;

import org.apache.commons.lang3.time.StopWatch;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

/**
 * @author dpetuhov
 */
public class ListTest {

    private static final Logger LOG = LogManager.getLogger();

    private static final long ITEM_COUNT = 1_000_000;
    private static final long ITER_COUNT = 10_000;

    private List<Integer> baseList = new ArrayList<>();

    @Before
    public void setUp() {
        populateList(baseList);
    }

    private static void populateList(@NotNull List<Integer> list) {
        Random rand = new Random(System.currentTimeMillis());
        list.clear();
        for (long i = 0; i < ITEM_COUNT; i++) {
            list.add(rand.nextInt((int) ITEM_COUNT));
        }
    }

    @Test
    public void testLinkedList() {
        testList(LinkedList.class);
    }

    @Test
    public void testArrayList() {
        testList(ArrayList.class);
    }

    @Test
    public void testVector() {
        testList(Vector.class);
    }

    private void testList(Class<? extends List> listClass) {
        LOG.info(listClass.getSimpleName());
        try {
            // Insertion
            List list = listClass.newInstance();
            testListInsert(list);

            // Retrieving
            list = listClass.newInstance();
            list.addAll(baseList);
            testListGetFirst(list);
            testListGetMiddle(list);
            testListGetLast(list);

        } catch (InstantiationException | IllegalAccessException e) {
            LOG.catching(e);
        }
    }

    private void testListInsert(List<Object> list) {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        for (Object item : baseList) {
            list.add(item);
        }
        stopWatch.stop();
        LOG.info(String.format("  Add: %1$d msec", stopWatch.getNanoTime() / 1_000, ITEM_COUNT));
    }

    private void testListGetFirst(List<Object> list) {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        int itemIdx = 1;
        for (int i = 0; i < ITER_COUNT; i++) {
            list.get(itemIdx);
        }
        stopWatch.stop();
        LOG.info(String.format("  Get(1): %1$d msec", stopWatch.getNanoTime() / 1_000, ITER_COUNT));
    }

    private void testListGetMiddle(List<Object> list) {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        int itemIdx = list.size() / 2;
        for (int i = 0; i < ITER_COUNT; i++) {
            list.get(itemIdx);
        }
        stopWatch.stop();
        LOG.info(String.format("  Get(N/2): %1$d msec", stopWatch.getNanoTime() / 1_000, ITER_COUNT));
    }

    private void testListGetLast(List<Object> list) {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        int itemIdx = list.size()-1;
        for (int i = 0; i < ITER_COUNT; i++) {
            list.get(itemIdx);
        }
        stopWatch.stop();
        LOG.info(String.format("  Get(N): %1$d msec", stopWatch.getNanoTime() / 1_000, ITER_COUNT));
    }


}
