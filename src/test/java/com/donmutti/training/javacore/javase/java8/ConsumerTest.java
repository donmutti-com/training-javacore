package com.donmutti.training.javacore.javase.java8;

import com.donmutti.training.javacore.pojo.Person;
import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Consumer;

import static org.junit.Assert.assertTrue;

/**
 * @author dpetuhov
 */
public class ConsumerTest {

    private static final long ITEM_COUNT = 100;

    private List<Person> baseList = new ArrayList<>();

    @Before
    public void setUp() {
        populateList(baseList);
    }

    private static void populateList(@NotNull List<Person> list) {
        Random rand = new Random(System.currentTimeMillis());
        list.clear();
        for (long i = 0; i < ITEM_COUNT; i++) {
            list.add(new Person((long) rand.nextInt(100), "item-" + i));
        }
    }

    @Test
    public void testConsumer() {
        Consumer<Person> consumer = p -> { p.setName(p.getName() + " Jr"); };
        List<Person> testList = new ArrayList<>(baseList);
        testList.stream().forEach(consumer);

        assertTrue(testList.stream().allMatch(person -> person.getName().contains("Jr")));
    }
}
