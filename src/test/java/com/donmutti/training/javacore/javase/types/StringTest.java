package com.donmutti.training.javacore.javase.types;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author dpetuhov
 */
public class StringTest {

    @Test
    public void testIntern() {
        assertTrue("ABC" == "ABC");  // String literals intern automatically
        assertTrue("ABC" != new String("ABC"));  // but not new String objects
        assertTrue(new String("ABC") != new String("ABC"));  // as the new operator always creates another instance
        assertTrue(new String("ABC").intern() != new String("ABC"));  // the same here
        assertTrue(new String("ABC") != new String("ABC").intern());  // and here
        assertTrue(new String("ABC").intern() == new String("ABC").intern());    // until we intern() both Strings explicitly
    }

    @Test
    public void testLeftPad() {
        final String base = "Hello world!";
        final String test = leftPad(base, 'A', 25);
        final String expected = "AAAAAAAAAAAAAHello world!";
        assertEquals(expected, test);
    }

    private String leftPad(String s, char padWith, int newLen) {
        int padLen = newLen - s.length();
        if (padLen > 0) {
            char[] pad = new char[padLen];
            Arrays.fill(pad, padWith);
            s = String.valueOf(pad).concat(s);
        }
        return s;
    }
}
