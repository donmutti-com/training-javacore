package com.donmutti.training.javacore.javase.types;

import com.donmutti.training.javacore.pojo.Direction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author dpetuhov
 */
public class EnumTest {

    private static final Logger LOG = LogManager.getLogger(EnumTest.class);

    @Test
    public void testEnumToString() {
        assertEquals(java.util.Arrays.toString(Direction.values()), "[NORTH, EAST, SOUTH, WEST]");
    }

    @Test
    public void testEnumCompare() {
        assertTrue(Direction.NORTH.compareTo(Direction.SOUTH) < 0);
    }

    @Test
    public void testEnumOrdinal() {
        assertEquals(Direction.WEST.ordinal(), 3);
    }

    @Test
    public void testEnumMethod() {
        assertEquals(Direction.WEST.turnRight(), Direction.NORTH);
    }

}