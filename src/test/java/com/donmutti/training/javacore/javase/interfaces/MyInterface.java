package com.donmutti.training.javacore.javase.interfaces;

interface MyInterface {

    static int doStatic() {
        return 10;
    }

    default int doDefault() {
        return 11;
    }

    int doAbstract();
}
