package com.donmutti.training.javacore.javase.types;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author dpetuhov
 */
public class NumberTest {

    private static final Logger LOG = LogManager.getLogger();

    @Before
    public void setUp() {
        LOG.info("Numbers test");

        double value = 12345.678E300;

        byte b = (byte) value;
        char c = (char) value;
        short s = (short) value;
        int i = (int) value;
        long l = (long) value;
        float f = (float) value;
        double d = (double) value;

        LOG.info("b = " + b);
        LOG.info("c = " + c);
        LOG.info("s = " + s);
        LOG.info("i = " + i);
        LOG.info("l = " + l);
        LOG.info("f = " + f);
        LOG.info("d = " + d);

        d = 0;
        for (i = 0; i < 10; i++) {
            d = d + 0.1;
            LOG.info(String.format("i: %s; d: %s", i, d));
        }

        float f1 = 0.0f;
        float f2 = -0.0f;
        LOG.info(f1 + " == " + f2 + " ? " + (f1 == f2));
        LOG.info("Float.compare(" + f1 + ", " + f2 + ") == " + Float.compare(f1, f2));

        f1 = Float.MAX_VALUE - 100;
        f2 = Float.MAX_VALUE + 100;
        LOG.info(f1 + " == " + f2 + " ? " + (f1 == f2));
        LOG.info("Float.compare(" + f1 + ", " + f2 + ") == " + Float.compare(f1, f2));

        f1 = 115;
        LOG.info("Float.floatToIntBits(" + f1 + ") == " + Float.floatToIntBits(f1));
        LOG.info("Float.floatToRawIntBits(" + f1 + ") == " + Float.floatToRawIntBits(f1));

        f1 = Float.MAX_VALUE;
        LOG.info("Float.MAX_VALUE == " + f1);
        LOG.info("Float.floatToRawIntBits(Float.MAX_VALUE) == " + Float.floatToRawIntBits(Float.MAX_VALUE));


        double bias = 1E291;
        double d1 = Double.MAX_VALUE - bias;
        double d2 = Double.MAX_VALUE + bias;
        LOG.info("Double.MAX_VALUE - " + bias + " = " + d1);
        LOG.info("Double.MAX_VALUE + " + bias + " = " + d2);
        LOG.info("Double.isInfinite(Double.MAX_VALUE - " + bias + ") = " + Double.isInfinite(d1));
        LOG.info("Double.isInfinite(Double.MAX_VALUE + " + bias + ") = " + Double.isInfinite(d2));
        LOG.info("d1 == d2 ? " + (d1 == d2));
        LOG.info("Double.compare(d1, d2) = " + Double.compare(d1, d2));
        LOG.info("NaN == NaN ? " + (Double.NaN == Double.NaN));
    }

    @Test
    public void testInt() {
        assertEquals(0, 3 / 10);
    }

    @Test
    public void testFloat() {
        // Positive/negative zero
        float f1 = 0.0f;
        float f2 = -0.0f;
        assertTrue(f1 == f2);
        assertTrue(Float.compare(f1, f2) > 0);

        // MIN_VALUE is positive
        assertTrue(0.0 < Float.MIN_VALUE);

        // MAX_VALUE +/-
        float bias = 100f;
        f1 = Float.MAX_VALUE - bias;
        f2 = Float.MAX_VALUE + bias;
        assertTrue(f1 == f2);
        assertTrue(Float.compare(f1, f2) == 0);

        // arithmetic
        assertEquals(0.3f, 3.0f / 10.0f, 0);
        assertEquals(0.3f, 3.0f * 0.1f, 0);
    }

    @Test
    public void testDouble() {
        // Positive/negative zero
        double d1 = 0.0;
        double d2 = -0.0;
        assertTrue(d1 == d2);
        assertTrue(Double.compare(d1, d2) > 0);

        // MIN_VALUE is positive
        assertTrue(0.0 < Double.MIN_VALUE);

        // MAX_VALUE +/-
        double bias = 1E291;
        d1 = Double.MAX_VALUE - bias;
        d2 = Double.MAX_VALUE + bias;
        assertTrue(!Double.isInfinite(d1));
        assertTrue(!Double.isInfinite(d2));
        assertTrue(d1 == d2);
        assertTrue(Double.compare(d1, d2) == 0);
        assertTrue(Double.NaN != Double.NaN);

        // arithmetic
        assertEquals(0.3, 3.0 / 10.0, 0);
        assertNotEquals(0.3, 3.0 * 0.1, 0);
    }
}