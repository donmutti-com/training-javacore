package com.donmutti.training.javacore.javase.regex;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RunWith(DataProviderRunner.class)
public class RegexTest {

    static Pattern PATTERN_FIND_URLS = Pattern.compile("(?i)(https?:\\/\\/|www)+[-\\w\\d+&@#/%?=~_|!:,.;]*[-\\w\\d+&@#/%=~_|]");

    @DataProvider
    public static Object[][] dataFindUrls() {
        return new Object[][]{
            {
                "This text doesn't contain URLs.",
                0
            },
            {
                "Hello www.google.com bye!",
                1
            },
            {
                "Hello www.google.com http://google.com bye!",
                2
            }
        };
    }

    @Test
    @UseDataProvider("dataFindUrls")
    public void testMatcherFind(String text, Integer expectedUrlCount) throws Exception {
        Matcher matcher = PATTERN_FIND_URLS.matcher(text);
        int actualUrlCount = 0;
        while (matcher.find()) ++actualUrlCount;
        Assert.assertEquals(expectedUrlCount.intValue(), actualUrlCount);
    }
}
