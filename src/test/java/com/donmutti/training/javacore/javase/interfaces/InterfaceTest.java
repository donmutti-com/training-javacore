package com.donmutti.training.javacore.javase.interfaces;

import org.junit.Assert;
import org.junit.Test;

public class InterfaceTest {

    MyInterface obj = new MyClass();

    @Test
    public void testInterfaceStatic() {
        Assert.assertEquals(10, MyInterface.doStatic());
    }

    @Test
    public void testClassStatic() {
        Assert.assertEquals(30, MyClass.doStatic());
    }

    @Test
    public void testDefault() {
        Assert.assertEquals(21, obj.doDefault());
    }

    @Test
    public void testAbstract() {
        Assert.assertEquals(32, obj.doAbstract());
    }


}

