package com.donmutti.training.javacore.javaee;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.FlagTerm;
import java.util.Date;
import java.util.Properties;

/**
 * If getting an AuthenticationFailedException, go to your Google Account and turn on "Access for less secure apps"
 * (<a href="https://www.google.com/settings/security/lesssecureapps">https://www.google.com/settings/security/lesssecureapps</a>)
 *
 * @author petukhovd
 * @see <a href="https://support.google.com/mail/answer/78754">https://support.google.com/mail/answer/78754</a>
 */
@Ignore("Don't want redundant emails in my inbox")
public class JavaMailTest {

    private static final Logger LOG = LogManager.getLogger();

    // Mailing params
    private static final String SEND_HOST = "smtp.gmail.com";
    private static final int SEND_PORT_SMTP_TLS = 587;
    private static final int SEND_PORT_SMTP_SSL = 465;

    private static final String RECEIVE_POP3_HOST = "pop.gmail.com";
    private static final int RECEIVE_POP3_PORT = 995;

    private static final String RECEIVE_IMAP_HOST = "imap.gmail.com";
    private static final int RECEIVE_IMAP_PORT = 993;

    private static final String TO_ADDRESS = "don.mutti@gmail.com";
    private static final String USER = "don.mutti@gmail.com";
    private static final String PASSWORD = "[et.gjgflfkjdj";

    @Test
    public void testSendTLS() throws MessagingException {
        Properties props = new Properties();
        props.put("mail.smtp.host", SEND_HOST);
        props.put("mail.smtp.port", SEND_PORT_SMTP_TLS);
        props.put("mail.smtp.auth", true);
        props.put("mail.smtp.starttls.enable", true);
        send(props, "Sent through TLS");
    }

    @Test
    public void testSendSSL() throws MessagingException {
        Properties props = new Properties();
        props.put("mail.smtp.host", SEND_HOST);
        props.put("mail.smtp.port", SEND_PORT_SMTP_SSL);
        props.put("mail.smtp.auth", true);
        props.put("mail.smtp.socketFactory.port", SEND_PORT_SMTP_SSL);
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        send(props, "Sent through SSL");
    }

    @Test
    public void testReceivePop3() throws MessagingException {
        Properties props = new Properties();
        props.put("mail.pop3.host", RECEIVE_POP3_HOST);
        props.put("mail.pop3.port", RECEIVE_POP3_PORT);
        receive(props, RECEIVE_POP3_HOST, RECEIVE_POP3_PORT, "pop3s", USER, PASSWORD);
    }

    @Test
    public void testReceiveImap() throws MessagingException {
        Properties props = new Properties();
        props.put("mail.imap.host", RECEIVE_IMAP_HOST);
        props.put("mail.imap.port", RECEIVE_IMAP_PORT);
        receive(props, RECEIVE_IMAP_HOST, RECEIVE_IMAP_PORT, "imaps", USER, PASSWORD);
    }

    private void send(Properties props, String subject) throws MessagingException {
        Authenticator auth = new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(USER, PASSWORD);
            }
        };
        Session session = Session.getInstance(props, auth);
        Message msg = new MimeMessage(session);
        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(TO_ADDRESS));
        msg.setSubject(subject);
        msg.setSentDate(new Date());

        String htmlText = "<html><body><h1>Hello there!</h1><p>This is a message text</p></body></html>";
        MimeBodyPart htmlPart = new MimeBodyPart();
        htmlPart.setContent(htmlText, "text/html; charset=utf-8");

        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(htmlPart);
        msg.setContent(multipart);

        Transport.send(msg);
    }

    private void receive(Properties props, String host, int port, String storeName, String user, String password) throws MessagingException {
        Session session = Session.getDefaultInstance(props);

        Store store = session.getStore(storeName);
        store.connect(host, port, user, password);

        Folder folder = store.getFolder("INBOX");
        folder.open(Folder.READ_ONLY);
        folder.getUnreadMessageCount();

        FlagTerm flagTerm = new FlagTerm(new Flags(Flags.Flag.SEEN), false);
        Message[] msgs = folder.search(flagTerm);
    }

}

