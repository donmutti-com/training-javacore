package com.donmutti.training.javacore.pojo;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.awt.*;

/**
 * @author dpetuhov
 */
public class Pen implements Comparable<Pen> {
    private static final Color DEFAULT_COLOR = Color.black;
    private static final float DEFAULT_VOLUME = 100.0f;

    private Color color;
    private float volume;

    public Pen() {
        this.color = DEFAULT_COLOR;
        this.volume = DEFAULT_VOLUME;
    }

    public Pen(Color color) {
        super();
        this.color = color;
    }

    public Pen(Color color, float volume) {
        this(color);
        this.volume = volume;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public float getVolume() {
        return volume;
    }

    public void setVolume(float volume) {
        this.volume = volume;
    }

    @Override
    public String toString() {
        return color + "/" + volume;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Pen)) {
            return false;
        } else if (obj == this) {
            return true;
        } else {
            Pen other = (Pen) obj;
            return new EqualsBuilder()
                .append(color, other.getColor())
                .append(volume, other.getVolume())
                .build();
        }
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
            .append(color)
            .append(volume)
            .build();
    }

    @Override
    public int compareTo(Pen o) {
        return (int) (volume * 100 - o.volume * 100);
    }
}
