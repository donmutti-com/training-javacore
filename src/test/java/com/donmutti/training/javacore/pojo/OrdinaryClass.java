package com.donmutti.training.javacore.pojo;

public class OrdinaryClass {

    public static Boolean doStaticBoolean() {
        return true;
    }

    public final Boolean doFinalBoolean() {
        return true;
    }

}
