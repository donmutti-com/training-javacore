package com.donmutti.training.javacore.pojo;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dpetuhov
 */
public class Person {
    private Long id;
    private String name;

    private List<Pet> pets = new ArrayList<>();

    public Person() {
    }

    public Person(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Person(Person item) {
        if (item != null) {
            this.id = item.getId();
            this.name = item.getName();
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Pet> getPets() {
        return pets;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
            .append(getId())
            .append(getName())
            .build();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !Person.class.isAssignableFrom(obj.getClass())) {
            return false;
        } else {
            Person other = (Person) obj;
            return new EqualsBuilder()
                .append(this.getId(), other.getId())
                .append(this.getName(), other.getName()).build();
        }
    }
}
