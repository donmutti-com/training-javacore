package com.donmutti.training.javacore.pojo;

/**
 * @author dpetuhov
 */
public enum Direction {

    NORTH {
        @Override public Direction turnRight() { return EAST; }
    },
    EAST {
        @Override public Direction turnRight() { return SOUTH; }
    },
    SOUTH {
        @Override public Direction turnRight() { return WEST; }
    },
    WEST {
        @Override public Direction turnRight() { return NORTH; }
    };

    public abstract Direction turnRight();

}
