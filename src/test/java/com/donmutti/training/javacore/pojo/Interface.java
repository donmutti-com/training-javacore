package com.donmutti.training.javacore.pojo;

public interface Interface {

    Boolean doBoolean();

    static Boolean doStaticBoolean() {
        return true;
    }

    default Boolean doDefaultBoolean() {
        return true;
    }

}
